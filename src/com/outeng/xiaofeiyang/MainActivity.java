package com.outeng.xiaofeiyang;



import java.io.File;
import java.util.HashMap;
import java.util.Properties;
import java.util.Set;

import org.xutils.common.util.FileUtil;

import com.tencent.smtt.sdk.CacheManager;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class MainActivity<JsPromptResult, JsResult> extends Activity {

	@Override
	 public void onCreate(Bundle savedInstanceState) {
	 super.onCreate(savedInstanceState);
	    //设置无标题   
   requestWindowFeature(Window.FEATURE_NO_TITLE);  
   

   
   //设置全屏   
//   getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,    
//           WindowManager.LayoutParams.FLAG_FULLSCREEN);   
	 setContentView(R.layout.main);
	 
	 
	 
	 iniview();
	 }
	
	WebView myWebView;
	
	/**
	 * url
	 */
	String strurl = "http://xfy178.cnhv6.hostpod.cn";
//	String strurl = "http://www.jzgclm.com/Api/Htmlview/good_detail/good_id/839";
//	String strurl = "https://www.taobao.com";
	
	
	 private static final String TAG = MainActivity.class.getSimpleName();  
	 
	 private static final String APP_CACAHE_DIRNAME = "/webcache";  
	 
	 
	 
	 
	 
	
	@SuppressLint("JavascriptInterface")
	public void iniview() {
		// TODO Auto-generated method stub
		myWebView = (WebView) findViewById(R.id.wv1);
		
		if (Build.VERSION.SDK_INT >= 19) {
			myWebView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
	    }
		
		myWebView.setVisibility(View.VISIBLE); // 显示这个WebView
		myWebView.requestFocus(); // 如果不设置，则在点击网页文本输入框时，不能弹出软键盘及不响应其他的一些事件。
		myWebView.setFocusableInTouchMode(true);
		myWebView.setFocusable(true);
		
//		myWebView.getSettings().setJavaScriptEnabled(true);
		// 与js交互，JavaScriptinterface 是个接口，与js交互时用到的，这个接口实现了从网页跳到app中的activity 的
		// 方法，特别重要
		myWebView.addJavascriptInterface(new JavaScriptinterface(this),"android");
//将图片下载阻塞
		myWebView.getSettings().setBlockNetworkImage(true); 
//		myWebView.loadUrl("http://m.taolvtuan.com");
//		myWebView.loadUrl("http://v.cyun58.com/mobile");
		
		
		myWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);//设置js可以直接打开窗口
		myWebView.getSettings().setJavaScriptEnabled(true);//是否允许执行js，默认为false。设置true时，会提醒可能造成XSS漏洞
		myWebView.getSettings().setSupportZoom(true);//是否可以缩放，默认true
		myWebView.getSettings().setBuiltInZoomControls(true);//是否显示缩放按钮，默认false
		myWebView.getSettings().setUseWideViewPort(true);//设置此属性，可任意比例缩放。大视图模式
		myWebView.getSettings().setLoadWithOverviewMode(true);//和setUseWideViewPort(true)一起解决网页自适应问题
		myWebView.getSettings().setAppCacheEnabled(false);//是否使用缓存
		myWebView.getSettings().setDomStorageEnabled(true);//DOM Storage
		
	
	    
		
		
		myWebView.loadUrl(strurl);
		
		myWebView.setWebViewClient(new myWebViewClient());
		
		myWebView.loadUrl("javascript:showAndroidToast()");
		
		
//		111
		myWebView.setWebChromeClient(new WebChromeClient() {
            // 拦截输入框(原理同方式2)
            // 参数message:代表promt（）的内容（不是url）
            // 参数result:代表输入框的返回值
            @SuppressLint("NewApi")
			public boolean onJsPrompt(WebView view, String url, String message, String defaultValue, JsPromptResult result) {
                // 根据协议的参数，判断是否是所需要的url(原理同方式2)
                // 一般根据scheme（协议格式） & authority（协议名）判断（前两个参数）
                //假定传入进来的 url = "js://webview?arg1=111&arg2=222"（同时也是约定好的需要拦截的）

                Uri uri = Uri.parse(message);
                // 如果url的协议 = 预先约定的 js 协议
                // 就解析往下解析参数
                if ( uri.getScheme().equals("js")) {

                    // 如果 authority  = 预先约定协议里的 webview，即代表都符合约定的协议
                    // 所以拦截url,下面JS开始调用Android需要的方法
                    if (uri.getAuthority().equals("webview")) {

                        //
                        // 执行JS所需要调用的逻辑
                        System.out.println("js调用了Android的方法");
                        // 可以在协议上带有参数并传递到Android上
                        HashMap<String, String> params = new HashMap<String, String>();
                        Set<String> collection = uri.getQueryParameterNames();

                        //参数result:代表消息框的返回值(输入值)
                        ((android.webkit.JsPromptResult) result).confirm("js调用了Android的方法成功啦");
                    }
                    return true;
                }
                return super.onJsPrompt(view, url, message, defaultValue, (android.webkit.JsPromptResult) result);
            }

//通过alert()和confirm()拦截的原理相同，此处不作过多讲述

            // 拦截JS的警告框
            public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                return super.onJsAlert(view, url, message, (android.webkit.JsResult) result);
            }

            // 拦截JS的确认框
            public boolean onJsConfirm(WebView view, String url, String message, JsResult result) {
                return super.onJsConfirm(view, url, message, (android.webkit.JsResult) result);
            }
        }
);
//		111end
		
		
		
		
		
	}
	
	
	public class myWebViewClient extends WebViewClient {

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			// TODO Auto-generated method stub
			
			
			//tel:18764421160
			
//			try {
				
			
//			if(url.length() > 10 ){
//				
//				//截取电话标识
//				String strtel = url.substring(0, 3);
//				if("tel".equals(strtel)){
//					
////					int dsd=url.length()-1;
////					String telcode = url.substring(4, url.length()-1);
//					
//					   Intent intent = new Intent(Intent.ACTION_CALL,Uri.parse(url));  
////					   Intent intent = new Intent(Intent.ACTION_CALL,Uri.parse("tel:"+telcode));  
//		                startActivity(intent);  
//		                
//		                return true;
//				}
//				
//				
//			}
//			
//			} catch (Exception e) {
//				// TODO: handle exception
//			}
			
			 // 获取上下文, H5PayDemoActivity为当前页面
			   final Activity context = MainActivity.this;
			 
			   // ------  对alipays:相关的scheme处理 -------
			   if(url.startsWith("alipays:") || url.startsWith("alipay")) {
			    try {
			     context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
			    } catch (Exception e) {
			     new AlertDialog.Builder(context)
			     .setMessage("未检测到支付宝客户端，请安装后重试。")
			     .setPositiveButton("立即安装", new DialogInterface.OnClickListener() {
			 
			      @Override
			      public void onClick(DialogInterface dialog, int which) {
			       Uri alipayUrl = Uri.parse("https://d.alipay.com");
			       context.startActivity(new Intent("android.intent.action.VIEW", alipayUrl));
			      }
			     }).setNegativeButton("取消", null).show();
			    }
			    return true;
			   }
			   // ------- 处理结束 -------
			 
			   if (!(url.startsWith("http") || url.startsWith("https"))) {
			    return true;
			   }
			   
			   
			   
//			   清理缓存 
			   if("http://xfy178.cnhv6.hostpod.cn/Home/Login/logout".equals(url)){
				   
//			    	清理缓存
			    	try {
						
					
			    		CleanMessageUtil.clearAllCache(getApplicationContext());
				   clearWebViewCache();
			    	 
			    	} catch (Exception e) {
						// TODO: handle exception
					}
			   }
			   
			   
			 
//			   myWebView.loadUrl(url);
//			   return true;
			   
			   
			
			view.loadUrl(url);
			
			return true;
		}
		
		@Override
		public void onPageFinished(WebView view, String url) {
			// TODO Auto-generated method stub
			myWebView.getSettings().setBlockNetworkImage(false); //通过图片的延迟载入，让网页能更快地显示。
			super.onPageFinished(view, url);
		}

	}
	
	
	
	/** 
     * 清除WebView缓存 
     */  
    public void clearWebViewCache(){  
          
        //清理Webview缓存数据库  
        try {  
            deleteDatabase("webview.db");   
            deleteDatabase("webviewCache.db");  
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
        
        
        try {
			
        //WebView 缓存文件  
        File appCacheDir = new File(getFilesDir().getAbsolutePath()+APP_CACAHE_DIRNAME);  
//        Log.e(TAG, "appCacheDir path="+appCacheDir.getAbsolutePath());  
          
        File webviewCacheDir = new File(getCacheDir().getAbsolutePath()+"/webviewCache");  
//        Log.e(TAG, "webviewCacheDir path="+webviewCacheDir.getAbsolutePath());  
          
        //删除webview 缓存目录  
        if(webviewCacheDir.exists()){  
            deleteFile(webviewCacheDir);  
        }  
        //删除webview 缓存 缓存目录  
        if(appCacheDir.exists()){  
            deleteFile(appCacheDir);  
        }  
        
        
        } catch (Exception e) {
        	// TODO: handle exception
        }
        
        
    }
    
    /** 
     * 递归删除 文件/文件夹 
     *  
     * @param file 
     */  
    public void deleteFile(File file) {  
  
//        Log.i(TAG, "delete file path=" + file.getAbsolutePath());  
          
    	try {
			
        if (file.exists()) {  
            if (file.isFile()) {  
                file.delete();  
            } else if (file.isDirectory()) {  
                File files[] = file.listFiles();  
                for (int i = 0; i < files.length; i++) {  
                    deleteFile(files[i]);  
                }  
            }  
            file.delete();  
        } else {  
//            Log.e(TAG, "delete file no exists " + file.getAbsolutePath());  
        } 
        
    	} catch (Exception e) {
    		// TODO: handle exception
    	}
    	
    }
    
    
    
    
    
	
	
	//实现点击两次返回键退出
	
	//【1】定义一个变量,用于标识是否退出
	boolean isExit;
	
	

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		 if ((keyCode == KeyEvent.KEYCODE_BACK) && myWebView.canGoBack()) {       
			 myWebView.goBack();       
	           return true;       
	        }     
		 
		  if (keyCode == KeyEvent.KEYCODE_BACK) {
		        exit();
		        return false;
		    } else {
		        return super.onKeyDown(keyCode, event);
		    }
		  
//	       return super.onKeyDown(keyCode, event);   
	}

	//【3】退出方法
	public void exit(){
			    if (!isExit) {
			        isExit = true;
			        Toast.makeText(getApplicationContext(), "再按一次退出程序", Toast.LENGTH_SHORT).show();
//			        mHandler.sendEmptyMessageDelayed(0, 2000);
			    } else {
			    	
//			    	清理缓存
			    	try {
						
					
			    		CleanMessageUtil.clearAllCache(getApplicationContext());
			    	 
			    	} catch (Exception e) {
						// TODO: handle exception
					}
			    	
//			    	清理缓存end
			    	
			    	
			        Intent intent = new Intent(Intent.ACTION_MAIN);
			        intent.addCategory(Intent.CATEGORY_HOME);
			        startActivity(intent);
			        System.exit(0);
			    }
			}
			
			
	//实现点击两次返回键退出 end
	
	
			
//	清理缓存
	
	
    
    
    
	
//	清理缓存END
			
			
			
	
}
