package com.outeng.xiaofeiyang;


import java.io.File;
import android.content.Context;
import android.os.Environment;


/**
 * 主要功能有清除内/外缓存，清除数据库，清除sharedPreference，清除files和清除自定义目录 
 * @author Administrator
 *
 */

public class DataCleanManager {

	
	 /** * 清除本应用内部缓存(/data/data/com.xxx.xxx/cache) * * @param context */
    public static void cleanInternalCache(Context context) {
    	
    	try {
			
    		deleteFilesByDirectory(context.getCacheDir());
		} catch (Exception e) {
			// TODO: handle exception
		}
    }

    /** * 清除本应用所有数据库(/data/data/com.xxx.xxx/databases) * * @param context */
    public static void cleanDatabases(Context context) {
    	
    	try {
			
    		deleteFilesByDirectory(new File("/data/data/"
    				+ context.getPackageName() + "/databases"));
		} catch (Exception e) {
			// TODO: handle exception
		}
    	
    }

    /**
     * * 清除本应用SharedPreference(/data/data/com.xxx.xxx/shared_prefs) * * @param
     * context
     */
    public static void cleanSharedPreference(Context context) {
    	
    	try {
			
    	
        deleteFilesByDirectory(new File("/data/data/"
                + context.getPackageName() + "/shared_prefs"));
    	} catch (Exception e) {
    		// TODO: handle exception
    	}
    }

    /** * 按名字清除本应用数据库 * * @param context * @param dbName */
    public static void cleanDatabaseByName(Context context, String dbName) {
    	try {
			
    		context.deleteDatabase(dbName);
		} catch (Exception e) {
			// TODO: handle exception
		}
    	
    }

    /** * 清除/data/data/com.xxx.xxx/files下的内容 * * @param context */
    public static void cleanFiles(Context context) {
    	
    	try {
			
    		deleteFilesByDirectory(context.getFilesDir());
		} catch (Exception e) {
			// TODO: handle exception
		}
    	
    }

    /**
     * * 清除外部cache下的内容(/mnt/sdcard/android/data/com.xxx.xxx/cache) * * @param
     * context
     */
    public static void cleanExternalCache(Context context) {
    	
    	try {
			
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            deleteFilesByDirectory(context.getExternalCacheDir());
        }
        
    	} catch (Exception e) {
    		// TODO: handle exception
    	}
    	
    	
    }

    /** * 清除自定义路径下的文件，使用需小心，请不要误删。而且只支持目录下的文件删除 * * @param filePath */
    public static void cleanCustomCache(String filePath) {
    	try {
			
    		deleteFilesByDirectory(new File(filePath));
		} catch (Exception e) {
			// TODO: handle exception
		}
    	
    }

    /** * 清除本应用所有的数据 * * @param context * @param filepath */
    public static void cleanApplicationData(Context context, String... filepath) {
    	
    	try {
			
		
        cleanInternalCache(context);
        cleanExternalCache(context);
        cleanDatabases(context);
        cleanSharedPreference(context);
        cleanFiles(context);
        for (String filePath : filepath) {
            cleanCustomCache(filePath);
        }
        
    	} catch (Exception e) {
			// TODO: handle exception
		}
    	
    	
    }

    /** * 删除方法 这里只会删除某个文件夹下的文件，如果传入的directory是个文件，将不做处理 * * @param directory */
    private static void deleteFilesByDirectory(File directory) {
    	
    	try {
			
        if (directory != null && directory.exists() && directory.isDirectory()) {
            for (File item : directory.listFiles()) {
                item.delete();
            }
        }
        
    	} catch (Exception e) {
    		// TODO: handle exception
    	}
    }
    
    
}
