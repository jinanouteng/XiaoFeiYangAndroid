package com.outeng.xiaofeiyang;

import java.util.HashMap;
import java.util.Set;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.webkit.WebChromeClient;
import android.widget.TextView;
import android.widget.Toast;

import com.outeng.xiaofeiyang.MainActivity.myWebViewClient;
import com.tencent.smtt.sdk.QbSdk;
import com.tencent.smtt.sdk.WebSettings;
import com.tencent.smtt.sdk.WebView;
import com.tencent.smtt.sdk.WebViewClient;
import com.tencent.smtt.sdk.WebSettings.LayoutAlgorithm;

public class X5WebView<JsPromptResult, JsResult> extends WebView{
	
	
	TextView title;
	
	/**
	 * 【C】
	 */
	public static Activity context;
	
	
	private WebViewClient client = new WebViewClient() {
		/**
		 * 防止加载网页时调起系统浏览器
		 */
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			
			
			 
			   // ------  对alipays:相关的scheme处理 -------
			   if(url.startsWith("alipays:") || url.startsWith("alipay")) {
			    try {
			     context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
			    } catch (Exception e) {
			     new AlertDialog.Builder(context)
			     .setMessage("未检测到支付宝客户端，请安装后重试。")
			     .setPositiveButton("立即安装", new DialogInterface.OnClickListener() {
			 
			      @Override
			      public void onClick(DialogInterface dialog, int which) {
			       Uri alipayUrl = Uri.parse("https://d.alipay.com");
			       context.startActivity(new Intent("android.intent.action.VIEW", alipayUrl));
			      }
			     }).setNegativeButton("取消", null).show();
			    }
			    return true;
			   }
			   // ------- 处理结束 -------
			 
			   if (!(url.startsWith("http") || url.startsWith("https"))) {
			    return true;
			   }
			   
			   
//			   清理缓存 
			   if("http://xfy178.cnhv6.hostpod.cn/Home/Login/logout".equals(url)){
				   
//			    	清理缓存
			    	try {
						
					
			    		CleanMessageUtil.clearAllCache(context);
			    	 
			    	} catch (Exception e) {
						// TODO: handle exception
					}
			   }
			   
			   
			   
			   
			
			view.loadUrl(url);
			
			return true;
		}
	};

	@SuppressLint("SetJavaScriptEnabled")
	public X5WebView(Context arg0, AttributeSet arg1) {
		super(arg0, arg1);
		this.setWebViewClient(client);
		// this.setWebChromeClient(chromeClient);
		// WebStorage webStorage = WebStorage.getInstance();
		initWebViewSettings();
		this.getView().setClickable(true);
	}

	private void initWebViewSettings() {
		WebSettings webSetting = this.getSettings();
		 // 设置与Js交互的权限
		webSetting.setJavaScriptEnabled(true);
		 // 设置允许JS弹窗
		webSetting.setJavaScriptCanOpenWindowsAutomatically(true);
		webSetting.setAllowFileAccess(true);
		webSetting.setLayoutAlgorithm(LayoutAlgorithm.NARROW_COLUMNS);
		webSetting.setSupportZoom(true);
		webSetting.setBuiltInZoomControls(true);
		webSetting.setUseWideViewPort(true);
		webSetting.setSupportMultipleWindows(true);
		// webSetting.setLoadWithOverviewMode(true);
		webSetting.setAppCacheEnabled(true);
		// webSetting.setDatabaseEnabled(true);
		webSetting.setDomStorageEnabled(true);
		webSetting.setGeolocationEnabled(true);
		webSetting.setAppCacheMaxSize(Long.MAX_VALUE);
		// webSetting.setPageCacheCapacity(IX5WebSettings.DEFAULT_CACHE_CAPACITY);
		webSetting.setPluginState(WebSettings.PluginState.ON_DEMAND);
		// webSetting.setRenderPriority(WebSettings.RenderPriority.HIGH);
		webSetting.setCacheMode(WebSettings.LOAD_NO_CACHE);
		
		
		

		// this.getSettingsExtension().setPageCacheCapacity(IX5WebSettings.DEFAULT_CACHE_CAPACITY);//extension
		// settings 的设计
	}

	@Override
	protected boolean drawChild(Canvas canvas, View child, long drawingTime) {
		boolean ret = super.drawChild(canvas, child, drawingTime);
//		canvas.save();
//		Paint paint = new Paint();
//		paint.setColor(0x7fff0000);
//		paint.setTextSize(24.f);
//		paint.setAntiAlias(true);
//		if (getX5WebViewExtension() != null) {
//			canvas.drawText(this.getContext().getPackageName() + "-pid:"
//					+ android.os.Process.myPid(), 10, 50, paint);
//			canvas.drawText(
//					"X5  Core:" + QbSdk.getTbsVersion(this.getContext()), 10,
//					100, paint);
//		} else {
//			canvas.drawText(this.getContext().getPackageName() + "-pid:"
//					+ android.os.Process.myPid(), 10, 50, paint);
//			canvas.drawText("Sys Core", 10, 100, paint);
//		}
//		canvas.drawText(Build.MANUFACTURER, 10, 150, paint);
//		canvas.drawText(Build.MODEL, 10, 200, paint);
//		canvas.restore();
		return ret;
	}

	public X5WebView(Context arg0) {
		super(arg0);
		setBackgroundColor(85621);
	}

	public void setWebChromeClient(WebChromeClient webChromeClient) {
		// TODO Auto-generated method stub
		
		Toast.makeText(context, "1111", 1).show();
		
	}

	

	

}
