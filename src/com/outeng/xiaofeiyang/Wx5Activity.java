package com.outeng.xiaofeiyang;

import java.util.HashMap;
import java.util.Set;

import com.outeng.xiaofeiyang.MainActivity.myWebViewClient;
import com.tencent.smtt.sdk.WebViewCallbackClient;
import com.tencent.smtt.sdk.WebViewClient;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.*;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.webkit.*;
import android.webkit.WebStorage.*;
import android.widget.Toast;
import android.net.*;
import android.net.http.*;
import android.os.Build;
import android.os.Bundle;

public class Wx5Activity<JsPromptResult, JsResult> extends Activity {

	
	@Override
	 public void onCreate(Bundle savedInstanceState) {
	 super.onCreate(savedInstanceState);
	    //设置无标题   
  requestWindowFeature(Window.FEATURE_NO_TITLE);  
  

  
  //设置全屏   
//  getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,    
//          WindowManager.LayoutParams.FLAG_FULLSCREEN);   
	 setContentView(R.layout.wx5);
	 
	 
	 
	 iniview();
	 
	 }
	
	//com.tencent.smtt.sdk.WebView
	X5WebView myWebView;
	
	String strurl = "http://xfy178.cnhv6.hostpod.cn";

	
	@SuppressLint("JavascriptInterface")
	private void iniview() {
		// TODO Auto-generated method stub
		
		
		myWebView = (X5WebView)findViewById(R.id.forum_context);
		
	
//		if (Build.VERSION.SDK_INT >= 19) {
//			myWebView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
//	    }
		

		
	
		Activity context = Wx5Activity.this;
		X5WebView.context = context;
		
		myWebView.loadUrl(strurl);
//		shouldOverrideUrlLoading
		
//		myWebView.setWebViewClient(new myWebViewClient());
		
		
//		111
		myWebView.setWebChromeClient(new WebChromeClient() {
            // 拦截输入框(原理同方式2)
            // 参数message:代表promt（）的内容（不是url）
            // 参数result:代表输入框的返回值
            @SuppressLint("NewApi")
			public boolean onJsPrompt(WebView view, String url, String message, String defaultValue, JsPromptResult result) {
                // 根据协议的参数，判断是否是所需要的url(原理同方式2)
                // 一般根据scheme（协议格式） & authority（协议名）判断（前两个参数）
                //假定传入进来的 url = "js://webview?arg1=111&arg2=222"（同时也是约定好的需要拦截的）

                Uri uri = Uri.parse(message);
                // 如果url的协议 = 预先约定的 js 协议
                // 就解析往下解析参数
                if ( uri.getScheme().equals("js")) {

                    // 如果 authority  = 预先约定协议里的 webview，即代表都符合约定的协议
                    // 所以拦截url,下面JS开始调用Android需要的方法
                    if (uri.getAuthority().equals("webview")) {

                        //
                        // 执行JS所需要调用的逻辑
                        System.out.println("js调用了Android的方法");
                        // 可以在协议上带有参数并传递到Android上
                        HashMap<String, String> params = new HashMap<String, String>();
                        Set<String> collection = uri.getQueryParameterNames();

                        //参数result:代表消息框的返回值(输入值)
                        ((android.webkit.JsPromptResult) result).confirm("js调用了Android的方法成功啦");
                    }
                    return true;
                }
                return super.onJsPrompt(view, url, message, defaultValue, (android.webkit.JsPromptResult) result);
            }

//通过alert()和confirm()拦截的原理相同，此处不作过多讲述

            // 拦截JS的警告框
            public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                return super.onJsAlert(view, url, message, (android.webkit.JsResult) result);
            }

            // 拦截JS的确认框
            public boolean onJsConfirm(WebView view, String url, String message, JsResult result) {
                return super.onJsConfirm(view, url, message, (android.webkit.JsResult) result);
            }
        }
);
//		111end
		
		
		
	}
	
	public class myWebViewClient extends WebViewClient {

		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			// TODO Auto-generated method stub
			
			
			//tel:18764421160
			
//			try {
				
			
//			if(url.length() > 10 ){
//				
//				//截取电话标识
//				String strtel = url.substring(0, 3);
//				if("tel".equals(strtel)){
//					
////					int dsd=url.length()-1;
////					String telcode = url.substring(4, url.length()-1);
//					
//					   Intent intent = new Intent(Intent.ACTION_CALL,Uri.parse(url));  
////					   Intent intent = new Intent(Intent.ACTION_CALL,Uri.parse("tel:"+telcode));  
//		                startActivity(intent);  
//		                
//		                return true;
//				}
//				
//				
//			}
//			
//			} catch (Exception e) {
//				// TODO: handle exception
//			}
			
			
			   
			   
			   
//			   清理缓存 
			   if("http://xfy178.cnhv6.hostpod.cn/Home/Login/logout".equals(url)){
				   
//			    	清理缓存
			    	try {
						
					
			    		CleanMessageUtil.clearAllCache(getApplicationContext());
			    	 
			    	} catch (Exception e) {
						// TODO: handle exception
					}
			   }
			   
			   
			 
//			   myWebView.loadUrl(url);
//			   return true;
			   
			   
			
			view.loadUrl(url);
			
			return true;
		}
		

	}
	
	
	
	//实现点击两次返回键退出
	
		//【1】定义一个变量,用于标识是否退出
		boolean isExit;
		
		

		@Override
		public boolean onKeyDown(int keyCode, KeyEvent event) {
			 if ((keyCode == KeyEvent.KEYCODE_BACK) && myWebView.canGoBack()) {       
				 myWebView.goBack();       
		           return true;       
		        }     
			 
			  if (keyCode == KeyEvent.KEYCODE_BACK) {
			        exit();
			        return false;
			    } else {
			        return super.onKeyDown(keyCode, event);
			    }
			  
//		       return super.onKeyDown(keyCode, event);   
		}

		//【3】退出方法
		public void exit(){
				    if (!isExit) {
				        isExit = true;
				        Toast.makeText(getApplicationContext(), "再按一次退出程序", Toast.LENGTH_SHORT).show();
//				        mHandler.sendEmptyMessageDelayed(0, 2000);
				    } else {
				    	
//				    	清理缓存
				    	try {
							
						
				    		CleanMessageUtil.clearAllCache(getApplicationContext());
				    	 
				    	} catch (Exception e) {
							// TODO: handle exception
						}
				    	
//				    	清理缓存end
				    	
				    	
				        Intent intent = new Intent(Intent.ACTION_MAIN);
				        intent.addCategory(Intent.CATEGORY_HOME);
				        startActivity(intent);
				        System.exit(0);
				    }
				}
				
				
		//实现点击两次返回键退出 end
		
		
	
	
	
}
